package ru.yam.atlas.jira.testreport.service;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.filter.SearchRequestService;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.query.Query;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import ru.yam.atlas.jira.testreport.model.ProjectStatistics;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static org.powermock.api.mockito.PowerMockito.when;
import static org.powermock.api.mockito.PowerMockito.whenNew;

@RunWith(PowerMockRunner.class)
@PrepareForTest({TestReportSearchServiceImpl.class, SearchService.ParseResult.class})
public class TestReportSearchServiceImplTest {

    private static SearchRequestService searchRequestService;
    private static SearchService searchService;
    private static JiraServiceContextImpl jiraServiceContext;
    private static SearchRequest searchRequest;
    private static ApplicationUser user;
    private static Query query;
    private static SearchService.ParseResult parseResult;
    private static String filterId = "10000";
    private static String period = "week";

    @BeforeClass
    public static void setup(){
        searchRequestService = mock(SearchRequestService.class);
        searchService = mock(SearchService.class);
        jiraServiceContext = mock(JiraServiceContextImpl.class);
        searchRequest = mock(SearchRequest.class);
        query = mock(Query.class);
        parseResult = PowerMockito.mock(SearchService.ParseResult.class);
        user = new MockApplicationUser("admin");

        when(parseResult.isValid()).thenReturn(true);
        when(parseResult.getQuery()).thenReturn(query);
        when(query.getQueryString()).thenReturn("query");
        when(searchRequest.getQuery()).thenReturn(query);

        final JiraAuthenticationContext jiraAuthenticationContext = mock(JiraAuthenticationContext.class);
        when(jiraAuthenticationContext.getUser()).thenReturn(user);
        new MockComponentWorker()
                .addMock(JiraAuthenticationContext.class, jiraAuthenticationContext)
                .init();
    }

    @Test
    public void testGetStatistics() throws Exception {
        Project project = mock(Project.class);
        Issue is = mock(Issue.class);
        when(is.getProjectObject()).thenReturn(project);
        when(project.getName()).thenReturn("project");
        when(project.getKey()).thenReturn("project");

        List<Issue> issueList = new ArrayList<>();
        issueList.add(is);

        SearchResults searchResults = mock(SearchResults.class);
        when(searchResults.getIssues()).thenReturn(issueList);
        when(searchService.search(any(User.class), eq(query), any(PagerFilter.class))).thenReturn(searchResults);

        TestReportSearchServiceImpl testReportSearchService = spy(new TestReportSearchServiceImpl(searchRequestService, searchService));
        doReturn(searchRequest).when(testReportSearchService).getSearchRequestByFilterId(filterId);
        doReturn(query).when(testReportSearchService).buildQueryWithPeriod(searchRequest, period);

        Collection<ProjectStatistics> statistics = testReportSearchService.getStatistics(filterId, period);

        assertEquals(1, statistics.size());
        ProjectStatistics element = statistics.iterator().next();
        assertEquals("project", element.getProjectKey());
        assertEquals("project", element.getProjectName());
        assertEquals(0, element.getClosedIssues());
        assertEquals(1, element.getTotalIssues());
    }

    @Test
    public void testGetIssueCountForFilter() throws Exception {
        when(searchService.searchCount(user.getDirectoryUser(), query)).thenReturn(1l);
        TestReportSearchServiceImpl testReportSearchService = spy(new TestReportSearchServiceImpl(searchRequestService, searchService));
        doReturn(query).when(testReportSearchService).buildQueryWithPeriod(searchRequest, period);

        long issueCountForFilter = testReportSearchService.getIssueCountForFilter(searchRequest, period);

        assertEquals(1, issueCountForFilter);
    }

    @Test
    public void testGetSearchRequestByFilterId() throws Exception {
        whenNew(JiraServiceContextImpl.class).withArguments(user).thenReturn(jiraServiceContext);
        when(searchRequestService.getFilter(jiraServiceContext, 10000l)).thenReturn(searchRequest);
        TestReportSearchServiceImpl testReportSearchService = new TestReportSearchServiceImpl(searchRequestService, searchService);

        SearchRequest searchRequestByFilterId = testReportSearchService.getSearchRequestByFilterId(filterId);

        assertEquals(searchRequest, searchRequestByFilterId);
    }

    @Test
    public void testBuildQueryWithPeriod() throws Exception {
        when(searchService.parseQuery(null, "query AND created > startOfWeek()")).thenReturn(parseResult);
        TestReportSearchServiceImpl testReportSearchService = new TestReportSearchServiceImpl(searchRequestService, searchService);

        Query result = testReportSearchService.buildQueryWithPeriod(searchRequest, period);

        assertEquals(query, result);
    }

}