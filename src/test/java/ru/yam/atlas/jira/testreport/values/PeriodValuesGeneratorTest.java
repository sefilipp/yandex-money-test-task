package ru.yam.atlas.jira.testreport.values;

import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.*;

public class PeriodValuesGeneratorTest {
    @Test
    public void testGetPeriodMap() throws Exception {
        Map<String, String> periodMap = PeriodValuesGenerator.getPeriodMap();

        assertEquals(3, periodMap.size());
        assertEquals("startOfWeek()", periodMap.get("week"));
        assertEquals("startOfMonth()", periodMap.get("month"));
        assertEquals("startOfYear()", periodMap.get("year"));
    }

    @Test
    public void testGetValues() throws Exception {
        PeriodValuesGenerator generator = new PeriodValuesGenerator();

        Map values = generator.getValues(null);

        assertEquals(3, values.size());
        assertEquals("week", values.get("week"));
        assertEquals("month", values.get("month"));
        assertEquals("year", values.get("year"));
    }

}