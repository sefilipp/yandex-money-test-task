package ru.yam.atlas.jira.testreport.model;

import org.junit.Test;

import static org.junit.Assert.*;

public class ProjectStatisticsTest {

    @Test
    public void testGettersAndSetters(){
        ProjectStatistics ps = new ProjectStatistics(null, null);

        ps.setProjectName("project");
        ps.setProjectKey("key");
        ps.setClosedIssues(1);
        ps.setTotalIssues(2);

        assertEquals("project", ps.getProjectName());
        assertEquals("key", ps.getProjectKey());
        assertEquals(1, ps.getClosedIssues());
        assertEquals(2, ps.getTotalIssues());

    }
    @Test
    public void testIncTotal() throws Exception {
        ProjectStatistics ps = new ProjectStatistics(null, null);
        ps.incClosed();
        assertEquals(1, ps.getClosedIssues());
    }

    @Test
    public void testIncClosed() throws Exception {
        ProjectStatistics ps = new ProjectStatistics(null, null);
        ps.incTotal();
        assertEquals(1, ps.getTotalIssues());
    }

}