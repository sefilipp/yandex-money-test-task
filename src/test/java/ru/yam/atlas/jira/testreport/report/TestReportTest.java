package ru.yam.atlas.jira.testreport.report;

import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.plugin.report.ReportModuleDescriptor;
import com.atlassian.jira.web.action.ProjectActionSupport;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.yam.atlas.jira.testreport.service.TestReportSearchServiceImpl;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class TestReportTest {

    private static TestReportSearchServiceImpl testReportSearchService;
    private static ProjectActionSupport projectActionSupport;
    private static ReportModuleDescriptor descriptor;
    private static Map<String, String> map;
    private static String filterId = "10000";
    private static String period = "week";

    @BeforeClass
    public static void setup(){
        testReportSearchService = mock(TestReportSearchServiceImpl.class);
        projectActionSupport = mock(ProjectActionSupport.class);
        descriptor = mock(ReportModuleDescriptor.class);
        map = new HashMap<>();
        map.put("period", period);
        map.put("filterid", filterId);
    }

    @Test
    public void testValidate() throws Exception {
        SearchRequest searchRequest = mock(SearchRequest.class);
        when(testReportSearchService.getSearchRequestByFilterId(filterId)).thenReturn(searchRequest);
        when(testReportSearchService.getIssueCountForFilter(searchRequest, period)).thenReturn(1l);

        TestReport report = new TestReport(testReportSearchService);

        report.validate(projectActionSupport, map);

        verify(testReportSearchService, times(1)).getSearchRequestByFilterId(filterId);
        verify(testReportSearchService, times(1)).getIssueCountForFilter(searchRequest, period);
    }

    @Test
    public void testGenerateReportHtml() throws Exception {
        when(descriptor.getHtml(eq("view"), anyMap())).thenReturn("report");
        when(testReportSearchService.getStatistics(filterId, period)).thenReturn(Collections.emptyList());
        TestReport report = new TestReport(testReportSearchService);
        report.init(descriptor);

        String html = report.generateReportHtml(projectActionSupport, map);

        assertEquals("report", html);
    }
}