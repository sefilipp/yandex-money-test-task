package ru.yam.atlas.jira.testreport.service;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.filter.SearchRequestService;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.query.Query;
import ru.yam.atlas.jira.testreport.model.ProjectStatistics;
import ru.yam.atlas.jira.testreport.values.PeriodValuesGenerator;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestReportSearchServiceImpl implements TestReportSearchService {

    private final SearchRequestService searchRequestService;
    private final SearchService searchService;

    public TestReportSearchServiceImpl(SearchRequestService searchRequestService, SearchService searchService) {
        this.searchRequestService = searchRequestService;
        this.searchService = searchService;
    }

    @Override
    public Collection<ProjectStatistics> getStatistics(String filterId, String period) throws SearchException {
        User user = ComponentAccessor.getJiraAuthenticationContext().getUser().getDirectoryUser();
        SearchRequest searchRequest = getSearchRequestByFilterId(filterId);
        Query resultQuery = buildQueryWithPeriod(searchRequest, period);

        SearchResults searchResults = searchService.search(user, resultQuery, PagerFilter.getUnlimitedFilter());

        Map<String, ProjectStatistics> statisticsMap = new HashMap<>();

        List<Issue> issues = searchResults.getIssues();
        for(Issue is : issues){
            String projectKey = is.getProjectObject().getKey();
            ProjectStatistics projectStatistics = statisticsMap.get(projectKey);
            //TODO: for Jira 7 we can use lambdas from Java 8
            //statisticsMap.computeIfAbsent(projectKey, k -> new ProjectStatistics(k, is.getProjectObject().getName()));
            if (projectStatistics == null) {
                statisticsMap.put(projectKey, new ProjectStatistics(projectKey, is.getProjectObject().getName()));
            }
            increaseStatisticsIssueCount(is, statisticsMap.get(projectKey));
        }

        return statisticsMap.values();
    }

    private ProjectStatistics increaseStatisticsIssueCount(Issue issue, ProjectStatistics statistics){
        statistics.incTotal();
        if (issue.getResolutionDate() != null) {
            statistics.incClosed();
        }
        return statistics;
    }

    @Override
    public long getIssueCountForFilter(SearchRequest request, String period) throws SearchException {
        User user = ComponentAccessor.getJiraAuthenticationContext().getUser().getDirectoryUser();
        Query resultQuery = buildQueryWithPeriod(request, period);
        return searchService.searchCount(user, resultQuery);
    }

    @Override
    public SearchRequest getSearchRequestByFilterId(String filterId) {
        JiraServiceContext ctx = new JiraServiceContextImpl(ComponentAccessor.getJiraAuthenticationContext().getUser());
        return searchRequestService.getFilter(ctx, Long.parseLong(filterId));
    }

    Query buildQueryWithPeriod(SearchRequest sr, String period){
        //TODO: replace with JqlQueryBuilder if it possible
        String queryString = sr.getQuery().getQueryString();
        queryString += " AND created > " + PeriodValuesGenerator.getPeriodMap().get(period);
        SearchService.ParseResult parseResult = searchService.parseQuery(null, queryString);
        if (parseResult.isValid()){
            return parseResult.getQuery();
        }
        return null;
    }
}
