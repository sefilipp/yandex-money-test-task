package ru.yam.atlas.jira.testreport.values;

import com.atlassian.configurable.ValuesGenerator;

import java.util.HashMap;
import java.util.Map;

public class PeriodValuesGenerator implements ValuesGenerator {

    private static final String CURR_WEEK = "week";
    private static final String CURR_MONTH = "month";
    private static final String CURR_YEAR = "year";

    private static final String JQL_CURR_WEEK = "startOfWeek()";
    private static final String JQL_CURR_MONTH = "startOfMonth()";
    private static final String JQL_CURR_YEAR = "startOfYear()";

    private static final Map<String, String> periodMap = new HashMap<>();
    static {
        periodMap.put(PeriodValuesGenerator.CURR_WEEK, JQL_CURR_WEEK);
        periodMap.put(PeriodValuesGenerator.CURR_MONTH, JQL_CURR_MONTH);
        periodMap.put(PeriodValuesGenerator.CURR_YEAR, JQL_CURR_YEAR);
    }

    public static Map<String, String> getPeriodMap() {
        return periodMap;
    }

    @Override
    public Map getValues(Map map) {
        Map<String, String> periods = new HashMap<>();

        //TODO: find way to use i18n for report properties
        periods.put(CURR_WEEK, "week");
        periods.put(CURR_MONTH, "month");
        periods.put(CURR_YEAR, "year");

        return periods;
    }
}
