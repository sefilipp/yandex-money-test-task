package ru.yam.atlas.jira.testreport.model;

public class ProjectStatistics {

    private String projectName;
    private String projectKey;
    private int totalIssues;
    private int closedIssues;

    public ProjectStatistics(String projectKey, String projectName) {
        this.projectKey = projectKey;
        this.projectName = projectName;
    }

    public void incTotal(){
        totalIssues++;
    }

    public void incClosed(){
        closedIssues++;
    }

    public String getProjectKey() {
        return projectKey;
    }

    public void setProjectKey(String projectKey) {
        this.projectKey = projectKey;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public int getTotalIssues() {
        return totalIssues;
    }

    public void setTotalIssues(int totalIssues) {
        this.totalIssues = totalIssues;
    }

    public int getClosedIssues() {
        return closedIssues;
    }

    public void setClosedIssues(int closedIssues) {
        this.closedIssues = closedIssues;
    }
}
