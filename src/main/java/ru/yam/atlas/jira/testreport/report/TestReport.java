package ru.yam.atlas.jira.testreport.report;

import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.plugin.report.impl.AbstractReport;
import com.atlassian.jira.web.action.ProjectActionSupport;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import ru.yam.atlas.jira.testreport.model.ProjectStatistics;
import ru.yam.atlas.jira.testreport.service.TestReportSearchService;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class TestReport extends AbstractReport {

    private static final Logger log = Logger.getLogger(TestReport.class);

    private final TestReportSearchService testReportSearchService;

    public TestReport(TestReportSearchService testReportSearchService) {
        this.testReportSearchService = testReportSearchService;
    }

    @Override
    public void validate(ProjectActionSupport action, Map params) {
        super.validate(action, params);

        String period = (String)params.get("period");
        String filterId = (String)params.get("filterid");

        if (StringUtils.isEmpty(filterId) || !StringUtils.isNumeric(filterId)) {
            log.warn("filter id not found");
            action.addError("filterid", action.getText("ru.yam.atlas.jira.testreport.report.filter.error"));
        } else {
            validateFilter(action, filterId, period);
        }
    }

    @Override
    public String generateReportHtml(ProjectActionSupport projectActionSupport, Map params) throws Exception {

        String period = (String)params.get("period");
        String filterId = (String)params.get("filterid");

        if (StringUtils.isEmpty(filterId) || !StringUtils.isNumeric(filterId)){
            log.warn("filter id not found");
            throw new RuntimeException("No filter provided");
        }

        Collection<ProjectStatistics> statistics = testReportSearchService.getStatistics(filterId, period);

        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("statistics", statistics);

        return descriptor.getHtml("view", resultMap);
    }

    private void validateFilter(ProjectActionSupport action, String filterId, String period) {

        SearchRequest searchRequest = testReportSearchService.getSearchRequestByFilterId(filterId);
        if (searchRequest == null) {
            log.warn(String.format("filter: %s is not valid", filterId));
            action.addErrorMessage(action.getText("ru.yam.atlas.jira.testreport.report.filter.error"));
            return;
        }

        long issueCountForFilter = 0;
        try {
            issueCountForFilter = testReportSearchService.getIssueCountForFilter(searchRequest, period);
        } catch (SearchException e) {
            action.addErrorMessage(action.getText("ru.yam.atlas.jira.testreport.report.filter.error"));
            return;
        }

        if (issueCountForFilter > 1000) {
            action.addErrorMessage(action.getText("ru.yam.atlas.jira.testreport.report.resultfilter.count.error"));
        }
    }
}
