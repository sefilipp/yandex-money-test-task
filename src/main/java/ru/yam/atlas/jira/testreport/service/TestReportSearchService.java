package ru.yam.atlas.jira.testreport.service;

import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchRequest;
import ru.yam.atlas.jira.testreport.model.ProjectStatistics;

import java.util.Collection;

public interface TestReportSearchService {

    Collection<ProjectStatistics> getStatistics(String filterId, String period) throws SearchException;

    long getIssueCountForFilter(SearchRequest request, String period) throws SearchException;

    SearchRequest getSearchRequestByFilterId(String filterId);
}
